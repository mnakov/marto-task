<?php
// src/Controller/GetTaskController.php
namespace App\Controller;

use App\Entity\Task;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class GetTaskController extends AbstractController {

  /**
   * @Route("/", name="tasks_list")
   */
  public function showTasks() {
    $repository = $this->getDoctrine()->getRepository(Task::class);
    $tasks = $repository->findAll();

    if (!$tasks) {
      throw $this->createNotFoundException(
        'Sorry, no tasks found!'
      );
    }

    return $this->render('task/tasks.html.twig', [
      'tasks' => $tasks,
    ]);
  }
}