<?php
// src/Controller/MyTaskController.php
namespace App\Controller;

use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class TaskController extends AbstractController {

  /**
   * @Route("/create-task", name="task_form")
   */

  public function new(Request $request) {

    $task = new Task();
    $task->setName('Write a task name');
    $task->setExecutionDate(new \DateTime('now'));

    $form = $this->createFormBuilder($task)
      ->add('name', TextType::class)
      ->add('ExecutionDate', DateType::class)
      ->add('save', SubmitType::class, ['label' => 'Create Task'])
      ->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {

      $task = $form->getData();
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($task);
      $entityManager->flush();
    }

    return $this->render('task/task.html.twig', [
      'form' => $form->createView(),
    ]);
  }
}
